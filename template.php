<?php

/*********************************************************************
 * This file includes theme function overrides, preprocessors or anything else.
 * Initializing the default values
 * check whether the variables are set or not. If they aren�t set, we need to set them to the default values. 
 * We accomplish that by retrieving one of the variables and seeing if it is null. 
 * If it is null, we save the defaults using variable_set() and then force the refresh of the settings in Drupal�s internals using theme_get_setting('', TRUE).
 * I recommend to pust this function on top of template.php
 */
if (is_null(theme_get_setting('drupalui_settings'))) {  // <-- change this line
  global $theme_key;

  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the theme-settings.php file.
   */
  $defaults = array(             // <-- change this array		
	// layout settings: collume, block theme width, menu type ......  	

	'drupalui_body_layout'					=> '1', // 1 | 2
	'drupalui_suckerfish'					=> '0', // 0 | 1
	'drupalui_jqueryslide'					=> '0', // 0 | 1
	'drupalui_jqueryslide_auto'					=> 'false', // true | false
	'drupalui_rtl'					=> '0', // 0 | 1
	// other settings	
	'drupalui_iepngfix'					=> '0', // 1|0	
	'drupalui_color'					=> '', // 1|0	
  );

  // Get default theme settings.
  $settings = theme_get_settings($theme_key);

  // Save default theme settings.
  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge($defaults, $settings)
  );
  // Force refresh of Drupal internals.
  theme_get_setting('', TRUE);
}

/**
 * get body class
 */
function phptemplate_body_class($left, $right) {
  if ($left != '' && $right != '') {
    $class = 'two-sidebars';
  }
  else {
    if ($left != '') {
      $class = 'sidebar-left';
    }
    if ($right != '') {
      $class = 'sidebar-right';
    }
  }

  if (isset($class)) {
    print ' class="'. $class .'"';
  }
}

/**
 * get node url for drupal theme gallery
 */
function phptemplate_preprocess_node(&$variables) {
  $node = $variables['node'];

  $variables['node_url']  = url('node/'. $node->nid);
  
  // Flatten the node object's member fields.
  $variables = array_merge((array)$node, $variables);
}



/**
 * Load css and js files
 */
drupal_add_css(drupal_get_path('theme', 'drupalui') .'/css/theme-gallery-body.css', 'theme');
 
 
if (theme_get_setting('drupalui_suckerfish') == '1') {
  drupal_add_css(drupal_get_path('theme', 'drupalui') .'/js/suckerfish/suckerfish.css', 'theme');
  drupal_add_js(drupal_get_path('theme', 'drupalui') .'/js/suckerfish/suckerfish.js', 'theme');
}

if (theme_get_setting('drupalui_jqueryslide')) {
  drupal_add_js(drupal_get_path('theme', 'drupalui') .'/js/easyslider/js/jquery.js', 'theme');
  drupal_add_js(drupal_get_path('theme', 'drupalui') .'/js/easyslider/js/easySlider1.5.js', 'theme');
}
if (theme_get_setting('drupalui_iepngfix')) {
  drupal_add_js(drupal_get_path('theme', 'drupalui') .'/js/jquery.pngFix.js', 'theme');
}

if (theme_get_setting('drupalui_rtl')) {
  drupal_add_css(drupal_get_path('theme', 'drupalui') .'/css/style_rtl.css', 'theme');
}

if (arg(0)=="taxonomy" && arg(1)=="term" && arg(2)=="1") {
   drupal_add_css(drupal_get_path('theme', 'drupalui') .'/css/theme-gallery.css', 'theme');
}



// End :P