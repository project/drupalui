<?php

if ($teaser): 
?>
<li>
	<h2><a href="<?php print $node_url ?>" title="<?php print $node->title ?>"><?php print $title ?></a></h2>
	<div class="date"><?php print format_date($node->created, 'custom', 'j-M-Y') ?></div>
	<div class="screenshot">
		<?php foreach ((array)$node->field_screenshot as $item) { ?>
		<a href="#1"><img src="<?php print $item['view'] ?>" width="220" height="300" /></a>
		<?php } ?>
	</div>
	<div class="comment_count"># comments: <?php print $node->comment_count ?></div>
	<div class="demo">
		<?php foreach ((array)$node->field_demo_link as $item) { ?>
		<a href="<?php print $item['view'] ?>" >Demo</a>
		<?php } ?>
	</div>
</li>
<?php else: ?>
<div class="theme_item">
	
	<div class="theme_image">
		<?php foreach ((array)$node->field_screenshot as $item) { ?>
		<?php print $item['view'] ?>
		<?php } ?>
	</div>
	
	<ul>
		<li class="theme-name"><span>Name: </span><?php print $title ?></li>
		<li class="theme-version"><span>Version: </span>6.x-1.0</li>
		<li class="theme-date"><span>Created date: </span><?php print format_date($node->created, 'custom', 'j-M-Y') ?></li>
		<li class="theme-demo">			
			<?php foreach ((array)$node->field_demo_link as $item) { ?>
			<span>View Demo: </span><?php print $item['view'] ?>
			<?php } ?>			
		</li>
		<li class="theme-tags"><span>Tags: </span><?php print $terms ?></li>
		<li class="theme-rattings"><span>Rattings: </span>5 stars </li>			
		<li class="theme-details">
			<span>Descriptions: </span><?php print $node->body ?>
		</li>
	</ul>
	
</div>
<div class="clear"></div>
<?php endif; ?>