<?php

// this file file for the advanced theme settings. http://drupal.org/node/177868
// An example themes/garland/theme-settings.php file.

function phptemplate_settings($saved_settings) {
  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the template.php file.
   */

  $defaults = array(
  //textfield | textarea | checkbox | radio | select
	// layout settings: collume, block theme width, menu type ......  	
	'drupalui_body_layout'					=> '1', // 1 | 2
	'drupalui_suckerfish'					=> '0', // 0 | 1
	'drupalui_jqueryslide'					=> '0', // 0 | 1
	'drupalui_jqueryslide_auto'					=> 'false', // true | false
	'drupalui_rtl'					=> '0', // 0 | 1
	// other settings	
	'drupalui_iepngfix'					=> '0', // 1|0	
	'drupalui_color'					=> '', 
  );

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);

  // Create the form widgets using Forms API
  $form['drupalui-div-opening'] = array(
    '#value'         => '<div id="drupalui-settings">',
  );
// ------------------------------------------------------------------------
$form['drupalui_layout'] = array(
	'#type'          => 'fieldset',
	'#title'         => t('Theme layout settings'),
	'#attributes'    => array('id' => 'drupalui-layout-settings'),
	'#collapsible' => true,
	'#collapsed' => true,
	'#description' => t('Layout settings, include: collume, block width, menu type ... '),
);

	$form['drupalui_layout']['drupalui_body_layout'] = array(
		'#type'          => 'radios',
		'#title'         => t('body layout type'),
		'#default_value' => $settings['drupalui_body_layout'],
		'#description'   => t('pick body layout'),
		'#options'       => array(
						'1'  => t('content-left-right > default'),
						'2'   => t('left-right-content'),
						  
							),
	);
	$form['drupalui_layout']['drupalui_suckerfish'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Enable suckerfish menu'),
		'#description'   => t('If you dont pick suckerfish menu, then the default menu will be use'),
		'#default_value' => $settings['drupalui_suckerfish'],		
	);
	$form['drupalui_layout']['drupalui_jqueryslide'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Enable jquery slider'),		
		'#default_value' => $settings['drupalui_jqueryslide'],		
	);
							
	$form['drupalui_layout']['drupalui_jqueryslide_auto'] = array(
		'#type'          => 'select',
		'#title'         => t('Jquery slide: auto or no ?'),
		'#description'   => t('Do you want to enable auto play for slider ? yes/no'),
		'#default_value' => $settings['drupalui_jqueryslide_auto'],
		'#options'       => array(
						'true'  => t('yes'),
						'false'   => t('no'),
							),
	);
	$form['drupalui_layout']['drupalui_rtl'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Enable RTL - Right to Left text'),
		'#description'   => t('Use RTL - Right to Left text ? '),
		'#default_value' => $settings['drupalui_rtl'],		
	);
// ------------------------------------------------------------------------

$form['drupalui_other'] = array(
	'#type'          => 'fieldset',
	'#title'         => t('other settings'),
	'#attributes'    => array('id' => 'drupalui-style-settings'),
	'#collapsible' => true,
	'#collapsed' => true,
	'#description' => t('Other settings, ex: iepngfix ... '),
);
	$form['drupalui_other']['drupalui_iepngfix'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Enable iepngFix (uncheck=disable=default)'),
		'#default_value' => $settings['drupalui_iepngfix'],
	);
	$form['drupalui_other']['drupalui_color'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Chose themes color'),
		'#description' => t('enter the color that you want to use. Available color: <b>blue|red|green|gray</b>'),
		'#default_value' => 'blue',		
	);
/*
	$form['drupalui_other']['meta_description'] = array(
		'#type' => 'textarea',
		'#title' => t('Meta description'),
		'#cols' => 60,
		'#rows' => 6,
		'#default_value' => $settings['meta_description'],
		'#prefix'        => '<div id="div-zen-wireframes"><strong>' . t('Wireframes:') . '</strong>',
		'#suffix'        => '</div>',
	);
*/
// ------------------------------------------------------------------------
  $form['drupalui-div-closing'] = array(
    '#value'         => '</div>',
  );
  // Return the additional form widgets
  return $form;
}
