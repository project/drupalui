$Id : README.txt


--- SUMMARY ---

Theme name: drupalui
Version: 6.x-1.0
Theme License: GNU
Author: design & code by pnxuan@gmail.com
Home Page: http://drupalui.com
Descriptions: clean design & tabless
	+ Fast page loading because simple and clearn design
	+ readable code, 
	+ valid xhtml/css
	+ support 3 color (not color module) - working in process
	+ tested on Firefox 3.0, firefox 3.5, opera, IE, Safari, Google Chrome
	+ settings page support
	+ RTL (right to left) is support and print.css also
	+ maintenance-page with nice forum style
	+ support suckerfish dropdown menu (4 level)
	+ 8 regions
	+ jquery.pngFix for IE
	
for more themes and services or support, pls visit my homepage
for any questions, pls mail me.

enjoy.
thanks



--- FAQs ---


	! you need to enable your theme, by go to this url: admin/build/themes

1/ how to enable the maintenance-page  ?
	A: open /sites/defaul/settings.php or /sites/your-site/settings.php file in your text editor
	
	search and uncomment it:
	
	~line 173
	$conf = array( 
	
	~line: 185
   'maintenance_theme' => 'drupalui',
	
	~linie 214
	);
	
	
2/ how to enable suckerfish menu and other stuff ?		
	Go to theme setting page at: admin/build/themes/settings/drupalui	
	scroll to the bottom of the page, and you can see options to enable them in "theme layout settings" and "other settings"
	
3/ How to use jquery slider ?
	a/ enable jquery slider in your theme's settings page admin/build/themes/settings/drupalui
	b/ Lets say you want to make 3 image slide ! Then you need to create 3 block, for each block you need to add this sample of content:
		
		<a href="#"><img src="sites/default/files/images/01.jpg" alt="test" /></a>
	
		

--- TO DO ---

1/ typo
2/ picture in comment and node
3/ profile style
3/ headline slide



--- KNOWS ISSUE ---

- problem: slider next - preview position
- jquery is not autostart so i add jquery at line 91 (file: template.php)