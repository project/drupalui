<?php
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block bottom_menu block-<?php print $block->module ?>">

<?php if (!empty($block->subject)): ?>
  <h5><?php print $block->subject ?></h5>
<?php endif;?>

  <div class="content"><?php print $block->content ?></div>
</div>
