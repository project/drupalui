﻿<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>
<title><?php print $head_title ?></title>
<!-- [Drupal Theme by pnxuan@gmail.com - contact me if you want my services :] -->
<?php
	print $head;
	print $styles;
	print $scripts;
	$drupalui_body_layout = theme_get_setting('drupalui_body_layout'); // 1 | 2
	$drupalui_color = theme_get_setting('drupalui_color'); 
?>

<?php if (theme_get_setting('drupalui_rtl')== '1') { ?>
<link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . path_to_theme() ?>/css/style_rtl.css" />
<?php } ?>
<!-- menu -->
	<link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . path_to_theme() ?>/js/jquerymenu/css/menu_style.css" />
	<!--[if lte IE 7]>
		<link rel="stylesheet" type="text/css" href="<?php print $base_path . path_to_theme() ?>/js/jquerymenu/css/ie.css" media="screen" />
	<![endif]-->
	<script type="text/javascript" language="javascript" src="<?php print $base_path . path_to_theme() ?>/js/jquerymenu/js/hoverIntent.js"></script>
	<script type="text/javascript" language="javascript" src="<?php print $base_path . path_to_theme() ?>/js/jquerymenu/js/jquery.dropdown.js"></script>
<!-- /menu -->
<link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . path_to_theme() ?>/css/color/<?php print $drupalui_color ?>.css" />
<?php if (theme_get_setting('drupalui_iepngfix')) { ?>
	<script type="text/javascript" src="<?php print $base_path . path_to_theme() ?>/js/jquery.pngFix.js"></script>	
	<script type="text/javascript">
	$(document).ready(function() {
		$(document).ready(function(){ 
			$(document).pngFix(); 
		}); 
	});
	</script>
<?php } ?>

</head>
<body id="drupalui_<?php print $drupalui_body_layout ?>" class="<?php print $body_classes ?>">
<a name="up"></a>
<div id="header">
	<div class="header_inner">
		<div class="logo">
			<h1>
				<a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><span><?php print t('Home') ?></span></a>
			</h1>
		</div>
		<?php if (theme_get_setting('drupalui_jquerymenu')== '1') { ?>
			<div id="jquerymenu">
			<?php print $jquerymenu; ?>
			</div><!-- /jquerymenu -->
		<?php } else { 
			print theme('links', $primary_links, array('class' => 'links primary-links'));
		} ?>	
	</div>
</div><!-- /header -->

<div id="headline">
	<div class="headline_inner clearfix">
		
	</div>
</div><!-- /headline -->

<div id="mainbody">
	<div class="mainbody_inner">
		
		<?php print $content ?>
		
	</div>
</div><!-- /mainbody -->

<div id="footer">
	<div class="footer_inner drupallogo clearfix">
		<?php print $footer_menu ?>
		<div class="copyright">
			<?php print $footer_message ?>
			<div class="go_top"><a href="#up" title="Go to top of the page">Go to top ▲</a></div>
		</div>
	</div>
</div><!-- /footer -->
<?php print $closure ?>
</body>
</html>