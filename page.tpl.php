﻿<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>
<title><?php print $head_title ?></title>
<!-- [Drupal Theme by pnxuan@gmail.com - contact me if you want my services :] -->
<?php
	print $head;
	print $styles;
	print $scripts;
	$drupalui_body_layout = theme_get_setting('drupalui_body_layout'); // 1 | 2
	$drupalui_color = theme_get_setting('drupalui_color'); 
	$drupalui_jqueryslide_auto = theme_get_setting('drupalui_jqueryslide_auto'); 
?>
<link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . path_to_theme() ?>/css/color/<?php print $drupalui_color ?>.css" />
<?php if (theme_get_setting('drupalui_iepngfix') || theme_get_setting('drupalui_jqueryslide')) { ?>
<script type="text/javascript">
$(document).ready(function() {
	<?php if (theme_get_setting('drupalui_iepngfix')) { ?>
	$(document).pngFix(); 
	<?php } ?>
	
	<?php if (theme_get_setting('drupalui_jqueryslide')) { ?>
	$("#slider").easySlider({
		auto: <?php print $drupalui_jqueryslide_auto ?>,
		continuous: false 
	});
	<?php } ?>
});
</script><?php } ?>
</head>
<body id="drupalui_<?php print $drupalui_body_layout ?>" class="<?php print $body_classes ?>">
<a name="up"></a>
<div id="header">
	<div class="header_inner">
		<div class="logo">
			<h1>
				<a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><span><?php print t('Home') ?></span></a>
			</h1>
		</div>
		<?php if (theme_get_setting('drupalui_suckerfish')== '1') { ?>
			<div id="suckerfish">
			<?php print $suckerfish; ?>
			</div><!-- /suckerfish -->
		<?php } else { 
			print theme('links', $primary_links, array('class' => 'links primary-links'));
		} ?>	
	</div>
</div><!-- /header -->

<div id="headline">
	<div class="headline_inner clearfix">
		<?php if ($headline) { ?>
		<div id="slider">
			<ul>
				<?php print $headline ?>				
			</ul>
		</div>
		<?php } ?>
	</div>
</div><!-- /headline -->

<div id="mainbody" class="clearfix">
	<div class="mainbody_inner">
		
		<div id="mainbody_content">
			<div class="mainbody_content_inner">
			<?php print $breadcrumb; ?>
			<?php print $content_top ?>
			
			<?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
			<?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
			<?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
			<?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
			<?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
			<?php if ($show_messages && $messages): print $messages; endif; ?>
			<?php print $help; ?>
			
			<?php if (!$is_front): //hidden content on frontpage ?>
			<div class="body_content">
			<?php print $content ?>
			</div><!-- /body content -->
			<?php endif; ?>
			
			<?php print $content_bottom ?>
			</div>
		</div>
		
		<div id="sidebars">
			<?php if ($sidebarsfull): ?>
			<div class="sidebarsfull">
				<?php print $sidebarsfull ?>
			</div>
			<?php endif; ?>
		
			<?php if ($left): ?>
			<div class="leftcol">
				<?php print $left ?>
			</div>
			<?php endif; ?>
			
			<?php if ($right): ?>
			<div class="rightcol">
				<?php print $right ?>
			</div>
			<?php endif; ?>
			
			<?php if ($sidebarsfullbot): ?>
			<div class="sidebarsfullbot">
				<?php print $sidebarsfullbot ?>
			</div>
			<?php endif; ?>
		</div><!-- /sidebars -->
		
	</div>
</div><!-- /mainbody -->

<div id="footer">
	<div class="footer_inner drupallogo clearfix">
		<?php print $footer_menu ?>
		<div class="copyright">
			<?php print $footer_message ?>
			<div class="go_top"><a href="#up" title="Go to top of page">Go to top ?</a></div>
		</div>
	</div>
</div><!-- /footer -->
<?php print $closure ?>
</body>
</html>