$Id: INSTALL.txt

Once you get Drupal installed and you start to come to terms with it you will probably want to customize the way it looks.

Installing a new theme is very straightforward:

   1. Download a new theme package. Note that themes for different Drupal versions are not compatible, version 5.x themes do not work with Drupal 6.x and reverse.
   2. Read any README or INSTALL files in the package to find out if there are any special steps needed for this theme.
   3. Upload the contents of the theme package to a new directory in the themes directory in your Drupal site. In Drupal 5.x & 6.x, you place your themes in /sites/all/themes/yourThemeName
   4. Click administer � themes and enable the new theme (Drupal will auto-detect its presence).
   5. Edit your user preferences and select the new theme. If you want it to be the default theme for all users, check the default box in the themes administration page.

   
More infomations about this subject, => http://drupal.org/node/456